var keys = document.querySelectorAll("#calculator span"); // Mygtukai iš html
var operators = ["+", "-", "x", "÷"];
var upperOperators = ["sin", "cos", "tan", "1 / x", "e^x", "x^2", "sqrt(x)", "|x|"];
var decimalAdded = false;
var nullOfC = true;

var upperFunction = false;
var upperIndex = 0;
var upperPow;

var input = document.querySelector(".screen");
input.innerHTML = "0";

// On Click
keys.forEach(element => {
    element.onclick = function(e){

        var input = document.querySelector(".screen");
		var inputVal = input.innerHTML;
        var btnVal = this.innerHTML;
        
        if (nullOfC){
            input.innerHTML = "";
            nullOfC = false;
        }

        if (btnVal == "C"){
            input.innerHTML = "0";
            decimalAdded = false;
            nullOfC = true;
            upperFunction = false;
            upperIndex = 0;
        }

        else if (btnVal == "="){

            if (upperFunction){
                switch (upperIndex){
                    case 0:
                        input.innerHTML = Math.sin(inputVal * Math.PI / 180).toFixed(10);
                        break;
                    case 1:
                        input.innerHTML = Math.cos(inputVal * Math.PI / 180).toFixed(10);
                        break;
                    case 2:
                        if (inputVal % 90 == 0) input.innerHTML = "NaN";
                        else input.innerHTML = Math.tan(inputVal * Math.PI / 180).toFixed(10);
                        break;
                    case 3:
                        input.innerHTML = eval ( "1/" + inputVal);
                        break;
                    case 4:
                        
                        input.innerHTML = Math.pow(Math.e, inputVal);
                        break;
                    case 5:
                        input.innerHTML = Math.pow(inputVal, 2);
                        break;
                    case 6:
                        input.innerHTML = Math.sqrt(inputVal);
                        break;
                    case 7:
                        input.innerHTML = Math.abs(inputVal);
                        break;
                }
            }

            else {
                var lygtis = inputVal;
                var lC = lygtis[lygtis.length - 1];
                lygtis = lygtis.replace(/x/g, "*").replace(/÷/g, "/");

                if (operators.lastIndexOf(lC) > -1 || lC == ".")
                    equation = equation.replace(/.$/, "");
                if (lygtis) input.innerHTML = eval(lygtis);
            }

            
        }

        else if (operators.indexOf(btnVal) > -1){
            if (!upperFunction){
                var lastChar = inputVal[inputVal.length - 1];
                if(inputVal != "" && operators.indexOf(lastChar) == -1) 
                    input.innerHTML += btnVal;
                else if(inputVal == "" && btnVal == "-") 
                    input.innerHTML += btnVal;
                if(operators.indexOf(lastChar) > -1 && inputVal.length > 1) {
				// Here, "." matches any character while $ denotes the end of string, so anything (will be an operator in this case) at the end of string will get replaced by new operator
                    input.innerHTML = inputVal.replace(/.$/, btnVal);
                    decimalAdded = false;
                }
            }
            else if(input.innerHTML == ""  && btnVal == "-"){
                    input.innerHTML += btnVal;
                    nullOfC = false;
            }
        }

        else if(btnVal == ".") {
			if(!decimalAdded) {
				input.innerHTML += btnVal;
				decimalAdded = true;
			}
        }

        else if (upperOperators.indexOf(btnVal) > -1){
            upperFunction = true;
            upperIndex = upperOperators.indexOf(btnVal);
            input.innerHTML = "0"
            nullOfC = true;
        }
        
        else {
			input.innerHTML += btnVal;
        }
        
        e.preventDefault();
    }
});
