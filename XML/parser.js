var xmlTag = "";
var xmlFile = "";

function butClick(){
  xmlTag = document.getElementById("xmlTag").value;
  xmlFile = document.getElementById("xmlFile").value;
  getRows();
  document.getElementById("genBut").disabled = true;
}

function getRows(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("get", xmlFile, true);
  xmlhttp.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200){
      showResult(this);
    }
  };
  xmlhttp.send(null);
}

function showResult(xmlhttp){
  var xmlDoc = xmlhttp.responseXML.documentElement;
  removeWhitespace(xmlDoc);
  var outputResult = document.getElementById("BodyRows");
  var rowData = xmlDoc.getElementsByTagName(xmlTag);
  addTableRowsFromXmlDoc(rowData, outputResult);
}

function addTableRowsFromXmlDoc(xmlNodes, tableNode){
  var table = tableNode.parentNode;
  var header = table.createTHead();
  var headRow = header.insertRow(0);
  console.log("Number of nodes: " + xmlNodes.length);
  for (var i = 0; i < xmlNodes.length; i++) {
    var newRow = tableNode.insertRow(i);
    newRow.className = (i%2) ? "OddRow" : "EvenRow";
    for (var j = 0; j < xmlNodes[i].childNodes.length; j++) {
      var newCell = newRow.insertCell(newRow.cells.length);
      if (i == 0){
        var cell = headRow.insertCell(headRow.cells.length);
        cell.outerHTML = "<th>" + xmlNodes[i].childNodes[j].nodeName + "</th>";
      }
      if (xmlNodes[i].childNodes[j].firstChild) {
        newCell.innerHTML = xmlNodes[i].childNodes[j].firstChild.nodeValue;
      } else {
        newCell.innerHTML = "-";
      }
      console.log("cell: " + newCell);
    }
  }
  table.appendChild(tableNode);
}

function removeWhitespace(xml){
  for (var i = 0; i < xml.childNodes.length; i++) {
    var currentNode = xml.childNodes[i];
    if (currentNode.nodeType == 1)
    {
      removeWhitespace(currentNode);
    }
    if (!(/\S/.test(currentNode.nodeValue)) && (currentNode.nodeType == 3))
    {
      xml.removeChild(xml.childNodes[i--]);
    }
  }
}
